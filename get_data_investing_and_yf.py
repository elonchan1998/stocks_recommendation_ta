# %%
import pandas as pd
import investpy
import pandas_datareader as web
import urllib.request
import requests
from bs4 import BeautifulSoup
from pathlib import Path
from functools import reduce
import os
from multiprocessing.dummy import Pool as ThreadPool
import logging
from itertools import repeat
import shutil
import yfinance as yf
from datetime import datetime, timedelta
# %%
def get_data(stock_list, start, end, country='US', append=False, verbose=False, parallel=True):
    """
    parameters:
    stock_list: list or str, list of stocks or single stock
    country: str, {HK, US, US_ETF}
    verbose: bool, show messages or not
    """
        
    # start2 = f'{start[-4:]}-{start[3:5]}-{start[0:2]}'
    # end2 = f'{end[-4:]}-{end[3:5]}-{end[0:2]}'

    stock_list = [stock_list] if isinstance(stock_list, str) else stock_list
    start_list = repeat(start) if isinstance(start, str) else start

    if parallel:
        with ThreadPool(16) as pool:
            pool.starmap(
                download_data,
                zip(
                    stock_list,
                    start_list,
                    repeat(end),
                    repeat(country),
                    repeat(append),
                    repeat(verbose)
                )
            )
    else:
        for code, start in zip(stock_list, start_list):
            download_data(code, start, end, country, append, verbose)

    ################################################################
    ## old code
    # error_list = []
    # for code in stock_list:
        # if verbose:
        #     print(code)
        # try:
        #     df = investpy.get_stock_historical_data(
        #         stock=code.replace('.', ''), 
        #         country=mapping[country], 
        #         from_date=start, 
        #         to_date=end).drop(columns='Currency')

        # except:
        #     # try to get data from yahoo finance
        #     if verbose:
        #         print(f'Cannot get {code} from investing.com. Try to get from Yahoo Finance.')

        #     code2 = code.replace('.', '-')
        #     code2 = code2 + '.HK' if country == 'HK' else code2
            
        #     try:
        #         df = web.DataReader(code2, start=start2, end=end2,
        #                     data_source='yahoo').drop(columns='Adj Close')
        #     except:
        #         print(f'Cannot get {code} from both sources.')
        #         error_list.append(code)
        #         continue
                            
        # file_name = Path() / 'Data' / country / f'{code}.csv' # f'./Data/{country}/{code}.csv')
        # try:
        #     if append:
        #         df.loc[start2:end2, :].to_csv(file_name, header=False, mode='a')
        #     else:
        #         df.to_csv(file_name)
        # except:
        #     print(f'Error occured at {file_name}')
    # pd.Series(error_list, name='ticker').to_csv(Path() / 'error_log' / f'error_list_{country}.csv')


def download_data(code, start, end, country, append, verbose):
    mapping = {'HK': 'hong kong', 'US': 'united states', 'US_ETF': 'united states'}
    if mapping.get(country) is None:
        print('Invalid country code. Country code should be US or HK.')
        return None

    start2 = f'{start[-4:]}-{start[3:5]}-{start[0:2]}'
    end2 = f'{end[-4:]}-{end[3:5]}-{end[0:2]}'
    try:
        df = investpy.get_stock_historical_data(
            stock=code.replace('.', ''), 
            country=mapping[country], 
            from_date=start, 
            to_date=end).drop(columns='Currency')

    except Exception as e:
        # try to get data from yahoo finance
        if verbose:
            print(f'Failed get {code} from investing.com. Encountered {e}. Try to get from Yahoo Finance.')

        code2 = code.replace('.', '-')
        code2 = code2 + '.HK' if country == 'HK' else code2
        
        try:
            df = yf.download(code2, start=start2, end=end2, threads=False, progress=False).drop(columns='Adj Close')
            # df = web.DataReader(code2, start=start2, end=end2,
            #             data_source='yahoo').drop(columns='Adj Close')
        except KeyError:
            logging.error(f'Failed to get {code} from both sources.')
            # print(f'Cannot get {code} from both sources.')
            return code
        except Exception as e:
            logging.error(e)
            return code

    # exit if no data is fetched
    if df.shape[0] == 0:
        return code

    file_name = Path() / 'Data' / country / f'{code}.csv' # f'./Data/{country}/{code}.csv')
    
    try:
        if append:
            df.loc[start2:end2, :].to_csv(file_name, header=False, mode='a')
        else:
            df.to_csv(file_name)
    except:
        logging.error(f'Failed to save {file_name}')
        return code
    else:
        print(f'{code} saved')
    
def get_hk_list(update=False):
    '''Return: numpy array of HK stock list'''
    stock_list_path = Path('./stock_list')
    if update:
        url = 'https://www.hkex.com.hk/eng/services/trading/securities/securitieslists/ListOfSecurities.xlsx'
        urllib.request.urlretrieve(url, stock_list_path / 'ListOfSecurities.xlsx')
    
    hk_list = pd.read_excel(stock_list_path / 'ListOfSecurities.xlsx', skiprows=2, dtype=str).query('Category == "Equity" | Category == "Exchange Traded Products"')['Stock Code'].to_numpy()
    return [code[1:] if code[0] == '0' else code for code in hk_list]

def get_us_list(stock_type, update=False):
    '''Parameters:
    stock_type: {'all', 'all_index', 'nasdaq', 'sp', 'dj', 'etf'}
    update: bool, if True, latest stock list will be downloaded from HKEX and nasdaqtrader and replace the existing file
    '''
    # tickers = [list(pd.read_table(f + '.txt')['Symbol']) for f in ['AMEX', 'NASDAQ', 'NYSE']]
    # tickers_flatten = [ticker for sublist in tickers for ticker in sublist ]
    # return tickers_flatten
    mapping = {'nasdaq': 'nasdaq100', 'sp': 'sp500', 'dj': 'dowjones'}
    files = ['nasdaqlisted.txt', 'otherlisted.txt']
    stock_list_path = Path('./stock_list')
    # Update raw files
    if update:
        urllib.request.urlretrieve(f'ftp://ftp.nasdaqtrader.com/SymbolDirectory/{files[0]}', stock_list_path / files[0])
        urllib.request.urlretrieve(f'ftp://ftp.nasdaqtrader.com/SymbolDirectory/{files[1]}', stock_list_path / files[1])
    
        res = reduce(lambda x,y: x.union(y), [set(get_index_list(val)) for val in mapping.values()])
        pd.Series(list(res), name='ticker').sort_values().to_csv(stock_list_path / 'us_index_list.csv')

    # Raw Data
    if stock_type in ('all', 'etf'):
        nasdaq = pd.read_table(stock_list_path / files[0], sep='|')[['Symbol', 'ETF']]
        other = pd.read_table(stock_list_path / files[1], sep='|')[['ACT Symbol', 'ETF']].rename(columns={'ACT Symbol': 'Symbol'})

        if stock_type == 'all':
            all = pd.concat([nasdaq, other]).dropna(subset=['Symbol']).sort_values('Symbol')
            return list(all['Symbol'])
        if stock_type == 'etf':
            etf = pd.concat([nasdaq, other]).query('ETF == "Y"').dropna(subset=['Symbol']).sort_values('Symbol')
            return list(etf['Symbol'])

    elif stock_type in mapping:  # get index stocks
        return get_index_list(mapping[stock_type])

    elif stock_type == 'all_index':
        index_tickers = list(pd.read_csv(stock_list_path / 'us_index_list.csv')['ticker'])
        return index_tickers
        
    else:
        print("Invalid index. Index should be either {'all', 'all_index', 'nasdaq', 'sp', 'dj', 'etf'}")
    
   
# Helper function for getting stocks in one index
def get_index_list(index):
    '''index: {nasdaq100, sp500, dowjones, all}'''
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    res = requests.get(f"https://www.slickcharts.com/{index}", headers=headers)
    soup = BeautifulSoup(res.content,'lxml')
    table = soup.find_all('table')[0] 
    return list(pd.read_html(str(table))[0].drop('#', axis=1)['Symbol'])

# def get_us_list(index='all', etf=False, update=False):
#     '''index: nasdaq, sp, dj, all'''
#     mapping = {'nasdaq': 'nasdaq100', 'sp': 'sp500', 'dj': 'dowjones'}

    # Helper function for getting stocks in one index
    # def get_index_list(index):
    #     '''index: {nasdaq100, sp500, dowjones, all}'''
    #     headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    #     res = requests.get(f"https://www.slickcharts.com/{mapping[index]}", headers=headers)
    #     soup = BeautifulSoup(res.content,'lxml')
    #     table = soup.find_all('table')[0] 
    #     return list(pd.read_html(str(table))[0].drop('#', axis=1)['Symbol'])

#     if update:
#         # Update raw file
#         res = reduce(lambda x,y: x.union(y), [set(get_index_list(key)) for key in mapping.keys()])
#         pd.Series(list(res), name='ticker').to_csv('us_list.csv')
    
#     index_tickers = list(pd.read_csv('us_list.csv')['ticker'])

#     if index == 'all' and not etf:
#         return sorted(index_tickers)
#     if index == 'all' and etf:
#         return sorted(index_tickers + get_us_etf_list(update))
#     if mapping.get(index) is not None:
#         return get_index_list(index.lower())
#     else:
#         print('Invalid index. Index should be nasdaq, sp or dj')

# def move_etf():
#     etf_list = set(get_us_etf_list())
#     for f in os.listdir(Path() / 'Data' / 'US'):
#         if f[:-4] in etf_list:
#             os.rename(Path() / 'Data' / 'US' / f, Path() / 'Data' / 'US_ETF' / f)
# %%
if __name__ == "__main__":
    # Initial set up:
    # inputs
    today = datetime.now().date()
    today = today.strftime('%d/%m/%Y')

    start, end = '01/01/2000', today
    update = True
    debug = True

    #############################################################
    hk_list = get_hk_list(update=update)
    us_list = get_us_list('all_index', update=update)  # {'all', 'all_index', 'nasdaq', 'sp', 'dj', 'etf'}
    us_etf_list = get_us_list('etf')
    print('Get stock list completed.')

    n_stock = [10, 10 , 10] if debug else [len(hk_list), len(us_list), len(us_etf_list)]

    initialize = input('Warning: initialization will remove all files in ./Data/HK, ./Data/US, and ./Data/US_ETF. y/n?')

    if initialize == 'y':
        paths = (Path('./Data/HK'), Path('./Data/US'), Path('./Data/US_ETF'))
        # Remove folders
        for path in paths:
            shutil.rmtree(path)
        # Create folders
        for path in paths:
            os.mkdir(path)

        get_data(hk_list[:n_stock[0]], start, end, 'HK')
        get_data(us_list[:n_stock[1]], start, end, 'US')
        get_data(us_etf_list[:n_stock[2]], start, end, 'US_ETF')

    else:
        print('Initialization aborted')