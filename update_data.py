#%%
import pandas as pd
import os
from collections import deque
import investpy
from datetime import datetime, timedelta
from get_data_investing_and_yf import get_hk_list, get_us_list, get_data
from pathlib import Path

today = datetime.now().date()
today = today.strftime('%d/%m/%Y')
# %% Check for stocks not in our list and delisted stocks
def check_not_existing_delisted(country, update=True):
    '''
    Check if there are new stocks not in Data and delisted stocks
    Parameters:
    country: HK, US, US_ETF
    update: bool, if True, latest stock list will be downloaded from HKEX and nasdaqtrader and replace the existing file
    '''
    path = Path() / 'Data' / country 
    existing_list = {ticker[:-4] for ticker in os.listdir(path)}
    if country == 'HK':
        latest_list = set(get_hk_list(update=update))
    elif country == 'US':
        latest_list = set(get_us_list('all_index', update=update))
    elif country == 'US_ETF':
        latest_list = set(get_us_list('etf', update=update))
    else:
        print('country can only be one of {HK, US, US_ETF}.')

    not_existing = latest_list - existing_list
    delisted = existing_list - latest_list

    if not_existing:
        get_data(list(not_existing), '01/01/2000', today, country)

        error_list = set(pd.read_csv(f'error_list_{country}.csv')['ticker'])
        new = not_existing - error_list
        added = new if len(new) > 0 else 'None'
        print(f'{added} added')

    if delisted:
        for ticker in delisted:
            os.remove(path / f'{ticker}.csv')
        print(f'{delisted} removed')

# %% Update data
def update_data(country, verbose=False):
    """
    Parameters:
    country: HK, US, US_ETF
    verbose: bool, show progress message or not
    """
    path = Path() / 'Data' / country

    last_day_list = []
    stock_list = []
    for ticker in os.listdir(path):

        ticker_num = ticker.replace('.csv', '')

        with open(path / ticker, 'r') as f:
            q = deque(f, 1)

        last_day = q[0].split(',')[0]
        last_day = datetime.strptime(last_day, '%Y-%m-%d') + timedelta(days=1)
        last_day = last_day.strftime('%d/%m/%Y')

        if last_day != today:
            last_day_list.append(last_day)
            stock_list.append(ticker_num)
        else:
            print(f'{ticker} is already up to date')
        # try:
        #     get_data(ticker_num, last_day, today, country, append=True)
        #     if verbose:
        #         print(ticker_num, ' Done')

        # except (ValueError, IndexError) as e:
        #     print(ticker_num)
        #     print(e)

    get_data(
        stock_list,
        start=last_day_list,
        end=today,
        country=country,
        append=True,
        verbose=verbose,
        parallel=True
    )

def remove_duplicate():
    for country in ('HK', 'US', 'US_ETF'):
        path = Path() / 'Data' / country
        for f in os.listdir(path):
            df = pd.read_csv(path / f, index_col=0)
            df[~df.index.duplicated()].to_csv(path / f)
        print(path, ' done')
# %%
if __name__ == "__main__":
    # check_not_existing_delisted('HK')
    # check_not_existing_delisted('US')
    # check_not_existing_delisted('US_ETF', update=False)

    update_data('HK')
    update_data('US')
    update_data('US_ETF')
