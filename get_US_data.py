# %%
import pandas as pd
import investpy
import os
# %%
# read stock tickers
nasdaq_tickers = pd.read_table('./nasdaq_tickers.csv')
nasdaq_tickers.drop(nasdaq_tickers.columns[[1,2,3,4,5]], axis=1, inplace=True)

# %%
path = './Data/US'
saved_us_tickers = [os.path.splitext(filename)[0] for filename in os.listdir(path)]

# %%
for row in nasdaq_tickers.itertuples():

    try:
        df = investpy.get_stock_historical_data(stock=row.SYMBOL,
                                                country='united states',
                                                from_date='01/01/2008',
                                                to_date='24/06/2020')

        df.to_csv('./Data/US/{}.csv'.format(row.SYMBOL))

    except RuntimeError as e:
        print(e)


# %%
