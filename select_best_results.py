# %%
import pickle
import os
from math import isnan
import talib
import re
import pandas as pd
import numpy as np


# %%
#Find the setting with largest probability
path = './Data/Result'

# init a dataframe col: strat, row: stock code

#{'strat': [stock_para, ...], ...}

temp_dict = {}
strat_name_list = []
i = 0
record = 0
thiscode=[]
for files in sorted(os.listdir(path)):

    strat_name = re.findall('.+?(?=[0-9])', files)[0][:-1]
    strat_name_list.append(strat_name)

    result = pickle.load(open(path + '/' + files, 'rb'))
    stock_code = list(result) #AMCR not exist

    maximums = []
    for i, code in enumerate(sorted(stock_code)):
        
        # Cheuk of all nan
        string_list = list(result[code].values())
        if all(value == 'nan' for value in ' '.join(map(str, string_list)).split(' ')):
            maximums.append('nan')
            continue

        # maximum = max(result[code], key=result[code].get)
        maximum_idx = np.nanargmax(list(result[code].values()))
        maximum = list(result[code].keys())[maximum_idx]
        max_value = result[code][maximum]
        maximums.append((maximum, max_value)) # should record the probability also

    temp_dict[files] = maximums

    print(files, 'Done')

# %%

df = pd.DataFrame.from_dict(temp_dict, orient='index')
df = df.transpose()

# %%
arr = np.arange(len(df.columns)) % 3

# %%
stock_code = list(pickle.load(open(path+'/'+'BB_Neg_20_HK.pickle', 'rb')))
df_HK = df.iloc[:len(stock_code), arr==0] 
df_HK.index = stock_code
df_HK.to_pickle('./Data/'+'best_results_HK.pickle')

stock_code = list(pickle.load(open(path+'/'+'BB_Neg_20_US.pickle', 'rb'))) # AMCR exist
df_US = df.iloc[:len(stock_code), arr==1]
df_US.index = stock_code
df_US.to_pickle('./Data/'+'best_results_US.pickle')

stock_code = list(pickle.load(open(path+'/'+'BB_Neg_20_US_ETF.pickle', 'rb')))
df_US_ETF = df.iloc[:len(stock_code), arr==2]
df_US_ETF.index = stock_code
df_US_ETF.to_pickle('./Data/'+'best_results_US_ETF.pickle')
# %%
