## Initializtion
This step will **remove all files** in `./Data_HK/`, `./Data_US`, `./Data_US_ETF` and download stock data
1. Open `get_data_investing_and_yf.py`
2. Set `update` and `debug`
    - `update`: bool, if true, download the latest stock list from HKEX and nasdaqtrader and replace the existing file
    - `debug`: bool, if true, only download first 10 stocks in HK, US and US_ETF
3. Run the file
4. A warning will pop up. Input `y` to the console.
5. Download process will start automatically. Wait until the programme finishes.

## Update data
This step will update the existing data under `./Data`
1. Run `update_data.py`
2. Wait until the programme finishes.


## Functions
`get_data(stock_list, start, end, country='US', append=False, verbose=False, parallel=True)`

Download stock data and save it in './Data/...'

| Parameter | Data Type | Description|
| ------ | ------ | ------ | 
| `stock_list` | list or str| list of stocks or single stock |
| `country` | str | one of {HK, US, US_ETF}|
|`start`|str|date in format 'DD/MM/YYYY'|
|`end`|str|date in format 'DD/MM/YYYY'|
|`append`|bool|Save data to existing file|
|`verbose` |bool |show messages or not |

**Returns**: None

---

`get_hk_list(update=False)`

Get HK stock list

| Parameter | Data Type | Description|
| ------ | ------ | ------ | 
| `update` | bool| if true, download the latest stock list from HKEX and replace the existing file|

**Returns**: list, HK stock list

---

`get_us_list(stock_type, update=False)`

Get US stock list

| Parameter | Data Type | Description|
| ------ | ------ | ------ | 
| `stock_type` | str| one of {'all', 'all_index', 'nasdaq', 'sp', 'dj', 'etf'} |
|`update` |bool|if true, download the latest stock list from nasdaqtrader and replace the existing file|

**Returns**: list, US stock list

---

`check_not_existing_delisted(country, update=True)`

Check if there are new stocks not in Data and delisted stocks

| Parameter | Data Type | Description|
| ------ | ------ | ------ | 
| `country` | str| one of {HK, US, US_ETF} |
|`update` | str| if true, download the latest stock list from HKEX or nasdaqtrader and replace the existing file|

**Returns**: None