# %%
import pickle
import os
from math import isnan
import talib
import re
import pandas as pd
import numpy as np
from datetime import datetime

# %%

df_HK = pickle.load(open('./Data/best_results_HK.pickle', 'rb'))
df_US = pickle.load(open('./Data/best_results_US.pickle', 'rb'))
df_US_ETF = pickle.load(open('./Data/best_results_US_ETF.pickle', 'rb'))

# %%
def check_signals(df, row):

    df = df[df['Close'] != 0] # drop all 0, for example 0001 2015/03/13

    columns = ['BB_Neg', 'BB_Pos', 'Big_Down', 'Big_Green_Candle', 'Big_Red_Candle', 'Big_Up', 'Break_N_High', 'Break_N_Low', 'Dragonfly_Doji', 'Gap_Down', 'Gap_Up', 'Golden_Cross', 'Gravestone_Doji', 'N_Down', 'N_Green_Candle', 'N_Red_Candle', 'N_Up', 'PPO', 'RSI', 'SMA']
    signals = pd.DataFrame(index=df.index, columns=columns)
    signals.loc[:, :] = 0.0

    # BB_Neg
    if row[0] != 'nan':
        temp = pd.DataFrame()
        temp['Upper'], temp['Middle'], temp['Lower'] = talib.BBANDS(df.Close, timeperiod=row[0][0][0], nbdevdn=row[0][0][1])

        signals.loc[df['Close'] > temp['Lower'], 'BB_Neg'] = 1

    # BB_Pos
    if row[1] != 'nan':
        temp = pd.DataFrame()
        temp['Upper'], temp['Middle'], temp['Lower'] = talib.BBANDS(df.Close, timeperiod=row[1][0][0], nbdevup=row[1][0][1])
        print((df['Close'] > temp['Upper']).sum())
        signals.loc[df['Close'] > temp['Upper'], 'BB_Pos'] = 1
        

    # Big_Down
    if row[2] != 'nan':
        signals.loc[(df['Close']/df['Close'].shift(1)-1)*100 < row[2][0], 'Big_Down'] = 1

    # Big_Green_Candle
    if row[3] != 'nan':
        signals.loc[(df['Close']/df['Open'] - 1) * 100 > row[3][0], 'Big_Green_Candle'] = 1

    # Big_Red_Candle
    if row[4] != 'nan':
        signals.loc[(df['Close']/df['Open'] - 1) * 100 < row[4][0], 'Big_Red_Candle'] = 1

    # Big_Up
    if row[5] != 'nan':
        signals.loc[(df['Close']/df['Close'].shift(1)-1)*100 > row[5][0], 'Big_Up'] = 1
    
    # Break_N_High
    if row[6] != 'nan':
        temp = pd.DataFrame()
        temp['Rolling_High'] = df['Close'].rolling(window=row[6][0]).max()
        
        signals.loc[df['Close'] > temp['Rolling_High'].shift(1), 'Break_N_High'] = 1

    # Break_N_Low
    if row[7] != 'nan':
        temp = pd.DataFrame()
        temp['Rolling_Low'] = df['Close'].rolling(window=row[7][0]).min()

        signals.loc[df['Close'] < temp['Rolling_Low'].shift(1), 'Break_N_Low'] = 1

    # Dragonfly_Doji
    if row[8] != 'nan':
        signals['Dragonfly_Doji'] = talib.CDLDRAGONFLYDOJI(df['Open'], df['High'], df['Low'], df['Close']).astype(int)

    # Gap_Down
    if row[9] != 'nan':
        signals.loc[(df['Open']/df['Close'].shift(1) - 1) * 100 < row[9][0], 'Gap_Down'] = 1

    # Gap_Up
    if row[10] != 'nan':
        signals.loc[(df['Open']/df['Close'].shift(1) - 1) * 100 > row[10][0], 'Gap_Up'] = 1

    # Golden_Cross
    if row[11] != 'nan':
        temp = pd.DataFrame()
        temp['Low_MA'] = talib.MA(df['Close'], timeperiod=row[11][0][0])
        temp['High_MA'] = talib.MA(df['Close'], timeperiod=row[11][0][1])
        
        signals.loc[temp['Low_MA'] > temp['High_MA'], 'Golden_Cross'] = 1

    # Gravestone_Doji
    if row[12] != 'nan':
        signals['Gravestone_Doji'] = talib.CDLGRAVESTONEDOJI(df['Open'], df['High'], df['Low'], df['Close'])

    # N_Down
    if row[13] != 'nan':
        temp = pd.DataFrame()
        temp['Close'] = df['Close'] # To set the temp's length
        temp['Lower_than_previous'] = 0
        temp.loc[df['Close'] < df['Close'].shift(1), 'Lower_than_previous'] = 1

        labels = temp['Lower_than_previous'].diff().ne(0).cumsum()
        signals['N_Down'] = (labels.map(labels.value_counts()) == row[13][0]).astype(int)

    # N_Green_Candle
    if row[14] != 'nan':
        temp = pd.DataFrame()
        temp['Close'] = df['Close']
        temp['Green_Candle'] = 0
        temp.loc[df['Close'] > df['Open'], 'Green_Candle'] = 1

        labels = temp['Green_Candle'].diff().ne(0).cumsum()
        signals['N_Green_Candle'] = (labels.map(labels.value_counts()) == row[14][0]).astype(int)

    # N_Red_Candle
    if row[15] != 'nan':
        temp = pd.DataFrame(columns=['Red_Candle'])
        temp['Close'] = df['Close']
        temp['Red_Candle'] = 0
        temp.loc[df['Close'] < df['Open'], 'Red_Candle'] = 1

        labels = temp['Red_Candle'].diff().ne(0).cumsum()
        signals['N_Red_Candle'] = (labels.map(labels.value_counts()) == row[15][0]).astype(int)

    # N_Up
    if row[16] != 'nan':
        temp = pd.DataFrame()
        temp['Close'] = df['Close']
        temp['Higher_than_previous'] = 0
        temp.loc[df['Close'] > df['Close'].shift(1), 'Higher_than_previous'] = 1

        labels = temp['Higher_than_previous'].diff().ne(0).cumsum()
        signals['N_Up'] = (labels.map(labels.value_counts()) == row[16][0]).astype(int)

    # PPO
    if row[17] != 'nan':
        temp = pd.DataFrame()
        temp['PPO'] = talib.PPO(df['Close'])

        signals.loc[temp['PPO'] > row[17][0], 'PPO'] = 1

    # RSI
    if row[18] != 'nan':
        temp = pd.DataFrame()
        temp['RSI'] = talib.RSI(df['Close'])

        signals.loc[temp['RSI'] > row[18][0], 'RSI'] = 1

    # SMA
    if row[19] != 'nan':
        temp = pd.DataFrame()
        temp['MA'] = talib.MA(df['Close'], timeperiod=row[19][0])

        signals.loc[df['Close'] > temp['MA'], 'SMA'] = 1


    signals = signals - signals.shift(1)
    signals = signals.clip(0)

    return signals.iloc[1:, :] # first row becomes Nan after shifting
# %%
column_names = ['Date', 'Stock', 'ETF', 'Strategy', 'Parameter', 'Signal', 'Past Signal Count', 'Profit Probability', 'Mean Return', 'Std Return', 'Min Return', '25 Percentile', '50 Percentile', '75 Percentile', 'Max Return']
today = datetime.today().strftime('%d/%m/%Y')

report_HK = pd.DataFrame(columns=column_names)
report_US = pd.DataFrame(columns=column_names)
report_US_ETF = pd.DataFrame(columns=column_names)
paths = ['./Data/HK/', './Data/US/', './Data/US_ETF/']

df_list = [df_HK, df_US, df_US_ETF]

for i, path in enumerate(paths):

    # if i != 1:
    #     continue

    for index, row in df_list[i].iterrows():

        # if index!='AMCR':
        #     continue
        print(index)
        stock_data = pd.read_csv(path+index+'.csv', index_col='Date')

        individual_report = pd.DataFrame(columns=column_names)

        signals = check_signals(stock_data, row)

        individual_report['Strategy'] = list(signals.columns)
        individual_report['Parameter'] = [x[0] if x != 'nan' else np.NaN for x in row.values]
        individual_report['Signal'] = signals.iloc[-1, :].transpose().values
        individual_report['Past Signal Count'] = signals.sum().values
        
        temp_df = (signals.mul(stock_data.Close.shift(-20), axis=0)/ signals.mul(stock_data.Close, axis=0).replace(0, np.nan) - 1)

        individual_report['Mean Return'] = temp_df.mean().values
        individual_report['Std Return'] = temp_df.std().values
        individual_report['Min Return'] = temp_df.min().values
        individual_report['25 Percentile'] = temp_df.quantile(q=0.25, numeric_only=False).values
        individual_report['50 Percentile'] = temp_df.quantile(q=0.5, numeric_only=False).values
        individual_report['75 Percentile'] = temp_df.quantile(q=0.75, numeric_only=False).values
        individual_report['Max Return'] = temp_df.max().values


        individual_report['Profit Probability'] = [x[1] if x !='nan' else np.NaN for x in row.values]

        individual_report['Date'] = today
        individual_report['Stock'] = index

        if i != 2:
            individual_report['ETF'] = False
        else:
            individual_report['ETF'] = True

        if i == 0:
            report_HK = report_HK.append(individual_report)
        if i == 1:
            report_US = report_US.append(individual_report)
        if i == 2:
            report_US_ETF = report_US_ETF.append(individual_report)

        # break

    print(path, 'done')

    
report_HK.reset_index(inplace=True, drop=True)
report_US.reset_index(inplace=True, drop=True)
report_US_ETF.reset_index(inplace=True, drop=True)

# %%

# 