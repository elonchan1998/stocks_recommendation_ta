# %%
import pandas as pd
import investpy
import os

# %%
path = './Data/HK'
saved_hk_tickers = [os.path.splitext(filename)[0] for filename in os.listdir(path)]
saved_hk_tickers = list(map(int, saved_hk_tickers))

# %%
for i in range(max(saved_hk_tickers), 51):

    try:
        df = investpy.get_stock_historical_data(stock=str(i).zfill(4),
                                                country='hong kong',
                                                from_date='01/01/2008',
                                                to_date='24/06/2020')

        df.to_csv('./Data/HK/{}.csv'.format(str(i).zfill(4)))

    except RuntimeError as e:
        print(e)


# %%
