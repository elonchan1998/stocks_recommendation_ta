import pandas as pd
import numpy as np
import talib
import os
from scipy import stats
import pickle
from pathlib import Path

class Strategy:

    def __init__(self, r=0):
        self.r = r
        self.location_paths = [('HK', './Data/HK'), ('US', './Data/US'), ('US_ETF', './Data/US_ETF')]

    def save_record(self, record, strat_name, location, shift_days):
        path = './Data/Result/'+strat_name+'_'+str(shift_days)+'_'+location+'.pickle'
        # path = Path(path) # uncommment if running on windows
        with open(path, 'wb') as f:
            pickle.dump(record, f)

    def SMA(self, strat_name='SMA', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue

                record_days = {}
                for ndays in range(2, 101):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly
                    df['SMA'] = talib.MA(df.Close, timeperiod=ndays)
                    df['Signal'] = 0
                    df.loc[df.Close > df['SMA'], 'Signal'] = 1
                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[ndays] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(self.r, np.inf)

                    record_days[ndays] = probability
                    
                record[stock_code] = record_days
                print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def Golden_Cross(self, strat_name='Golden_Cross', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days= {}
                for ndays in range(2,51):
                    for mdays in range(ndays+5, 52):
                        df = df_copy.copy()
                        df['low_MA'] = talib.MA(df.Close, timeperiod=ndays)
                        df['high_MA'] = talib.MA(df.Close, timeperiod=mdays)
                        df['Signal'] = 0
                        df.loc[df['low_MA'] > df['high_MA'], 'Signal']= 1
                        df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                        df.loc[df['Signal'] < 0, 'Signal'] = 0
                        df.dropna(inplace=True)

                        future_returns = df['Signal'] * df['Future_Return']
                        future_returns.dropna(inplace=True)
                        future_returns = future_returns[future_returns != 0]

                        if len(future_returns) <= 30:
                            record_days[(ndays, mdays)] = np.NaN
                            continue

                        density = stats.gaussian_kde(future_returns)
                        probability = density.integrate_box_1d(self.r, np.inf)

                        record_days[(ndays, mdays)] = probability

                record[stock_code] = record_days

                        # print(ndays, mdays, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)
            print('Golden Cross Done')

    def PPO(self, strat_name='PPO', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
            
                record_days = {}
                for ppo_para in range(-3, 4, 1):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['PPO'] = talib.PPO(df.Close) #default 12 26

                    df['Signal'] = 0
                    df.loc[df['PPO'] > ppo_para, 'Signal'] = 1
                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[ppo_para] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[ppo_para] = probability
                    
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def RSI(self, strat_name='RSI', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for rsi_para in range(5, 100, 5):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['RSI'] = talib.RSI(df.Close) #default 14

                    df['Signal'] = 0
                    df.loc[df['RSI'] > rsi_para, 'Signal'] = 1
                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[rsi_para] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[rsi_para] = probability
                    
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')
            
            self.save_record(record, strat_name, location, shift_days)

    def BB_Pos(self, strat_name='BB_Pos', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}

            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]

                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for bb_days in range(5, 55, 5):
                    for bb_sd in np.linspace(1.0, 2.0, 11):
                        df = df_copy.copy() # avoid df.dropna drop date repeatedly

                        df['Upper'], df['Middle'], df['Lower'] = talib.BBANDS(df.Close, timeperiod=bb_days, nbdevup=bb_sd)
                        df['Signal'] = 0
                        df.loc[df['Close'] > df['Upper'], 'Signal'] = 1
                        df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                        df.loc[df['Signal'] < 0, 'Signal'] = 0
                        df.dropna(inplace=True)

                        future_returns = df['Signal'] * df['Future_Return']
                        future_returns.dropna(inplace=True)
                        future_returns = future_returns[future_returns != 0]

                        if len(future_returns) <= 30:
                            record_days[(bb_days, bb_sd)] = np.NaN
                            continue

                        density = stats.gaussian_kde(future_returns)
                        probability = density.integrate_box_1d(0, np.inf)

                        record_days[(bb_days, bb_sd)] = probability
                        
                record[stock_code] = record_days
                print(i, stock_code, 'Done')
            
            self.save_record(record, strat_name, location, shift_days)

    def BB_Neg(self, strat_name='BB_Neg', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for bb_days in range(5, 55, 5):
                    for bb_sd in np.linspace(1.0, 2.0, 11):
                        df = df_copy.copy() # avoid df.dropna drop date repeatedly

                        df['Upper'], df['Middle'], df['Lower'] = talib.BBANDS(df.Close, timeperiod=bb_days, nbdevdn=bb_sd)

                        df['Signal'] = 0
                        df.loc[df['Close'] > df['Lower'], 'Signal'] = 1
                        df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                        df.loc[df['Signal'] < 0, 'Signal'] = 0
                        df.dropna(inplace=True)

                        future_returns = df['Signal'] * df['Future_Return']
                        future_returns.dropna(inplace=True)
                        future_returns = future_returns[future_returns != 0]

                        if len(future_returns) <= 30:
                            record_days[(bb_days, bb_sd)] = np.NaN
                            continue

                        density = stats.gaussian_kde(future_returns)
                        probability = density.integrate_box_1d(0, np.inf)

                        record_days[(bb_days, bb_sd)] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def Big_Up(self, strat_name='Big_Up', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for up_pct in np.arange(2, 5.5, 0.5):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    df.loc[(df['Close']/df['Close'].shift(1)-1)*100 > up_pct, 'Signal'] = 1
                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[up_pct] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[up_pct] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')
            
            self.save_record(record, strat_name, location, shift_days)

    def Big_Down(self, strat_name='Big_Down', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for down_pct in np.arange(-2, -5.5, -0.5):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    df.loc[(df['Close']/df['Close'].shift(1)-1)*100 < down_pct, 'Signal'] = 1
                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[down_pct] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[down_pct] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def N_Up(self, strat_name='N_Up', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()
                
                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for ndays in range(2, 6):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    
                    df['Higher_than_previous'] = 0
                    df.loc[df['Close'] > df['Close'].shift(1), 'Higher_than_previous'] = 1

                    labels = df['Higher_than_previous'].diff().ne(0).cumsum()
                    df['Signal'] = (labels.map(labels.value_counts()) == ndays).astype(int)

                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[ndays] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[ndays] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def N_Down(self, strat_name='N_Down', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for ndays in range(2, 6):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    
                    df['Lower_than_previous'] = 0
                    df.loc[df['Close'] < df['Close'].shift(1), 'Lower_than_previous'] = 1

                    labels = df['Lower_than_previous'].diff().ne(0).cumsum()
                    df['Signal'] = (labels.map(labels.value_counts()) == ndays).astype(int)

                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[ndays] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[ndays] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def Break_N_High(self, strat_name='Break_N_High', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for ndays in range(5, 51):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    df['Rolling_High'] = df['Close'].rolling(window=ndays).max()
                    df.loc[df['Close'] > df['Rolling_High'].shift(1), 'Signal'] = 1

                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[ndays] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[ndays] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def Break_N_Low(self, strat_name='Break_N_Low', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for ndays in range(5, 51):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    df['Rolling_High'] = df['Close'].rolling(window=ndays).min() # should be rolling low, typo only
                    df.loc[df['Close'] < df['Rolling_High'].shift(1), 'Signal'] = 1

                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[ndays] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[ndays] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def Big_Green_Candle(self, strat_name='Big_Green_Candle', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for body_pct in range(3, 7):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    df.loc[(df['Close']/df['Open'] - 1 )*100 > body_pct, 'Signal'] = 1

                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[body_pct] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[body_pct] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def Big_Red_Candle(self, strat_name='Big_Red_Candle', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for body_pct in range(-3, -7, -1):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    df.loc[(df['Close']/df['Open'] - 1 )*100 < body_pct, 'Signal'] = 1

                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[body_pct] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[body_pct] = probability

                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def N_Green_Candle(self, strat_name='N_Green_Candle', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for ndays in range(3, 11):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    df.loc[df['Close'] > df['Open'], 'Green_Candle'] = 1

                    labels = df['Green_Candle'].diff().ne(0).cumsum()
                    df['Signal'] = (labels.map(labels.value_counts()) == ndays).astype(int)

                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[ndays] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[ndays] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def N_Red_Candle(self, strat_name='N_Red_Candle', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for ndays in range(3, 11):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    df.loc[df['Close'] < df['Open'], 'Red_Candle'] = 1

                    labels = df['Red_Candle'].diff().ne(0).cumsum()
                    df['Signal'] = (labels.map(labels.value_counts()) == ndays).astype(int)

                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[ndays] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[ndays] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def Gap_Up(self, strat_name='Gap_Up', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for gap_pct in range(3, 11):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    df.loc[(df['Open']/df['Close'].shift(1) - 1) * 100 > gap_pct, 'Signal'] = 1

                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[gap_pct] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[gap_pct] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def Gap_Down(self, strat_name='Gap_Down', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for gap_pct in range(-3, -11, -1):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    df.loc[(df['Open']/df['Close'].shift(1) - 1) * 100 < gap_pct, 'Signal'] = 1

                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[gap_pct] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[gap_pct] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def Gravestone_Doji(self, strat_name='Gravestone_Doji', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for temp in range(1, 2):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    df['Signal'] = talib.CDLGRAVESTONEDOJI(df['Open'], df['High'], df['Low'], df['Close'])

                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[temp] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[temp] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)

    def Dragonfly_Doji(self, strat_name='Dragonfly_Doji', shift_days=20):
        for location, path in self.location_paths:
            path = Path(path)
            record = {}
            for i, filename in enumerate(sorted(os.listdir(path))):
                stock_code = os.path.splitext(filename)[0]
                print(stock_code)
                df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
                df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
                df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
                df.dropna(inplace=True)
                df_copy = df.copy()

                if df_copy.shape[0] < 100:
                    continue
                
                record_days = {}
                for temp in range(1, 2):
                    df = df_copy.copy() # avoid df.dropna drop date repeatedly

                    df['Signal'] = 0
                    df['Signal'] = talib.CDLDRAGONFLYDOJI(df['Open'], df['High'], df['Low'], df['Close'])

                    df['Signal'] = df['Signal'] - df['Signal'].shift(1)
                    df.loc[df['Signal'] < 0, 'Signal'] = 0
                    df.dropna(inplace=True)

                    future_returns = df['Signal'] * df['Future_Return']
                    future_returns.dropna(inplace=True)
                    future_returns = future_returns[future_returns != 0]

                    if len(future_returns) <= 30:
                        record_days[temp] = np.NaN
                        continue

                    density = stats.gaussian_kde(future_returns)
                    probability = density.integrate_box_1d(0, np.inf)

                    record_days[temp] = probability
                        
                record[stock_code] = record_days
                # print(i, stock_code, 'Done')

            self.save_record(record, strat_name, location, shift_days)