# %%
import pandas as pd
import numpy as np
import talib
import os
from scipy import stats
import pickle
from pathlib import Path

# %%
from Strategy import Strategy

# %% No need to run this part
# shift_days = 20
# for location, path in [('HK', './Data/HK'), ('US', './Data/US')]:
#     path = Path(path)
#     record = {}

#     if location != 'US':
#         continue

#     for i, filename in enumerate(sorted(os.listdir(path))):
#         stock_code = os.path.splitext(filename)[0]

#         if stock_code != 'AMCR':
#             continue

#         print(stock_code)
#         print(filename)
#         df = pd.read_csv(Path(f'./Data/{location}/{filename}'), index_col=0)
#         df = df[df.Close != 0]  # drop all 0, for example 0001 2015/03/13
#         df['Future_Return'] = df.Close.shift(-shift_days)/df.Close - 1
#         df.dropna(inplace=True)
#         df_copy = df.copy()
#         if df_copy.shape[0] < 100:
#             continue
        
#         record_days = {}
#         for bb_days in range(5, 55, 5):
#             for bb_sd in np.linspace(1.0, 2.0, 11):
#                 df = df_copy.copy() # avoid df.dropna drop date repeatedly

#                 df['Upper'], df['Middle'], df['Lower'] = talib.BBANDS(df.Close, timeperiod=5, nbdevup=1.3)
#                 df['Signal'] = 0
#                 df.loc[df['Close'] > df['Upper'], 'Signal'] = 1
#                 df['Signal'] = df['Signal'] - df['Signal'].shift(1)
#                 df.loc[df['Signal'] < 0, 'Signal'] = 0
#                 df.dropna(inplace=True)

#                 future_returns = df['Signal'] * df['Future_Return']
#                 future_returns.dropna(inplace=True)
#                 future_returns = future_returns[future_returns != 0]

#                 if len(future_returns) <= 30:
#                     record_days[(bb_days, bb_sd)] = np.NaN
#                     continue

#                 density = stats.gaussian_kde(future_returns)
#                 probability = density.integrate_box_1d(0, np.inf)

#                 record_days[(bb_days, bb_sd)] = probability
                
#             record[stock_code] = record_days
#         print(i, stock_code, 'Done')
# %%
strategist = Strategy()
# %%
strategist.SMA()
strategist.Golden_Cross()
strategist.PPO()
strategist.RSI()
strategist.BB_Pos()
strategist.BB_Neg()
strategist.Big_Up()
strategist.Big_Down()
strategist.N_Up()
strategist.N_Down()
strategist.Break_N_High()
strategist.Break_N_Low()
strategist.Big_Green_Candle()
strategist.Big_Red_Candle()
strategist.N_Green_Candle()
strategist.N_Red_Candle()
strategist.Gap_Up()
strategist.Gap_Down()
strategist.Gravestone_Doji()
strategist.Dragonfly_Doji()

